<?php

namespace app\admin\controller\order;

use app\common\controller\AdminController;
use EasyAdmin\annotation\ControllerAnnotation;
use EasyAdmin\annotation\NodeAnotation;
use think\App;

/**
 * @ControllerAnnotation(title="订单列表")
 */
class Lists extends AdminController
{

    use \app\admin\traits\Curd;

    public function __construct(App $app)
    {
        parent::__construct($app);

        $this->model = new \app\admin\model\OrderLists();
        
        $this->assign('getOrderStatusList', $this->model->getOrderStatusList());

        $this->assign('getShippingStatusList', $this->model->getShippingStatusList());

        $this->assign('getPayStatusList', $this->model->getPayStatusList());

        $this->assign('getPromTypeList', $this->model->getPromTypeList());

    }

    
}
