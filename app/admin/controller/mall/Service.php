<?php

namespace app\admin\controller\mall;

use app\common\controller\AdminController;
use EasyAdmin\annotation\ControllerAnnotation;
use EasyAdmin\annotation\NodeAnotation;
use think\App;

/**
 * @ControllerAnnotation(title="商城服务管理")
 */
class Service extends AdminController
{

    use \app\admin\traits\Curd;

    public function __construct(App $app)
    {
        parent::__construct($app);

        $this->model = new \app\admin\model\MallService();
        
    }

    //获取服务时长
    public function getServerSpan(){
        $data = [
            'code'  => 1,
            'msg'   => '',
            'data'  => config('field.mall_service_time_span'),
        ];
        return json($data);
    }
    
}
