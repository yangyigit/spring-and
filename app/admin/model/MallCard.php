<?php

namespace app\admin\model;

use app\common\model\TimeModel;

class MallCard extends TimeModel
{

    protected $name = "mall_card";

    protected $deleteTime = "delete_time";

    
    
    public function getTypeList()
    {
        return ['1'=>'次卡','2'=>'时卡','3'=>'通卡',];
    }


}