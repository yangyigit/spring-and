<?php

namespace app\admin\model;

use app\common\model\TimeModel;

class OrderLists extends TimeModel
{

    protected $name = "order_lists";

    protected $deleteTime = false;

    
    
    public function getOrderStatusList()
    {
        return ['0'=>'未确认','2'=>'已确认','3'=>'已取消',];
    }

    public function getShippingStatusList()
    {
        return ['0'=>'未发货','2'=>'已发货',];
    }

    public function getPayStatusList()
    {
        return ['0'=>'未支付','2'=>'已支付','3'=>'已退款',];
    }

    public function getPromTypeList()
    {
        return ['1'=>'服务','2'=>'充值','3'=>'产品',];
    }


}