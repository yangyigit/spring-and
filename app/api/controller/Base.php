<?php


namespace app\api\controller;


use app\BaseController;
use Firebase\JWT\JWT;

class Base extends BaseController
{
    protected $uid = null;
    protected $token = '';

    public function initialize()
    {
        $this->token = $this->request->header('token');
    }

    /**
     * 验证用户是否合法
     * @return bool
     */
    protected function checkUser()
    {
        $token = $this->request->header('token');
        if(empty($token)){
            return false;
        }
        $secret = config('app.app_secret');
        $decoded = JWT::decode($token, $secret, array('HS256'));
        $this->uid = $decoded->uid;
        return true;
    }
}
