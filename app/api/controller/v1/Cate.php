<?php
declare (strict_types = 1);

namespace app\api\controller\v1;

use app\api\controller\Base;
use think\facade\Db;

class Cate extends Base
{
    public function getCate()
    {
        $result = $this->validate(
            $data = [
                "type" => $this->request->param('type'),
                "token" => $this->token,
            ],
            $rule = [
                "type" => "require|number",
                "token" => "require",
            ],
            $msg = [
                "type" => "type不能为空",
                "token" => "token不能为空",
            ]
        );
        if (true !== $result) {
            // 验证失败 输出错误信息
            return jsonReturn([], '参数不完整', 400);
        }

        $initArr[] = [
            'id'=> '',
            'title'=> '全部'
        ];
        $cateList = Db::name('mall_cate')
                    ->field('id,title,status,sort,image')
                    ->where('type',$data['type'])
                    ->select();
        foreach ($cateList as $title){
            array_push($initArr,$title);
        }
        if(!$cateList){
            return jsonReturn([], '数据查询异常', 400);
        }
        return jsonReturn($initArr, 'ok', 200);
    }
}
