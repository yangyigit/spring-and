<?php
declare (strict_types = 1);

namespace app\api\controller\v1;

use app\api\controller\Base;
use think\facade\Db;

class Configuration extends Base
{
    public function getWebInfo()
    {
        $result = $this->validate(
            $data = [
                "token" => $this->token,
            ],
            $rule = [
                "token" => "require",
            ],
            $msg = [
                "token" => "token不能为空",
            ]
        );
        if (true !== $result) {
            // 验证失败 输出错误信息
            return jsonReturn([], '参数不完整', 400);
        }


        $configInfo = Db::name('system_config')
            ->field('name,value,remark')
            ->where('group','site')
            ->select();

        if(!$configInfo){
            return jsonReturn([], '数据查询异常', 400);
        }
        return jsonReturn($configInfo, 'ok', 200);
    }
}
