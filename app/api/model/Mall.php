<?php

// +----------------------------------------------------------------------
// | EasyAdmin
// +----------------------------------------------------------------------
// | PHP交流群: 763822524
// +----------------------------------------------------------------------
// | 开源协议  https://mit-license.org 
// +----------------------------------------------------------------------
// | github开源项目：https://github.com/zhongshaofa/EasyAdmin
// +----------------------------------------------------------------------

namespace app\api\model;


use app\common\model\TimeModel;
use think\facade\Db;

class Mall extends TimeModel
{

    protected $table = "";

    protected $deleteTime = 'delete_time';
    protected $createTime = 'create_time';
    protected $updateTime = 'update_time';


    public function getMallTable($type){
        switch ($type){
            case 1:
                return 'mall_service';
                break;
            case 2:
                return 'mall_card';
                break;
            case 3:
                return 'mall_goods';
                break;
            default :
                return '';
        }
    }

    public function getList($mallTable,$pageNo,$pageSize,$catId){

        $where = [];
        if($catId){
            $where=[
                'cate_id' => $catId
            ];
        }
        $list = Db::name($mallTable)
            ->page(intval($pageNo),intval($pageSize))
            ->where($where)
            ->select();
        $total = Db::name($mallTable)
            ->where($where)
            ->count();
        $data = [
            'list'=>$list,
            'total'=>$total
        ];
        return $data;
    }

    public function getDetails($mallTable,$where){

        if($mallTable){
            $list = Db::name($mallTable)
                ->where($where)
                ->find();
        }

        return $list;
    }

    public function getGoodsList($where,$pageNo=1,$pageSize=10)
    {
        $list = Db::name('mall_goods')
            ->page(intval($pageNo),intval($pageSize))
            ->where($where)
            ->select();
        $total = Db::name('mall_goods')
            ->where($where)
            ->count();
        $data = [
            'list'=>$list,
            'total'=>$total
        ];
        return $data;

    }

    public function getServiceList($where,$pageNo=1,$pageSize=10)
    {
        $list = Db::name('mall_service')
            ->page(intval($pageNo),intval($pageSize))
            ->where($where)
            ->select();
        $total = Db::name('mall_service')
            ->where($where)
            ->count();
        $data = [
            'list'=>$list,
            'total'=>$total
        ];
        return $data;

    }

    public function getCardList($where,$pageNo=1,$pageSize=10)
    {
        $list = Db::name('mall_card')
            ->page(intval($pageNo),intval($pageSize))
            ->where($where)
            ->select();
        $total = Db::name('mall_card')
            ->where($where)
            ->count();
        $data = [
            'list'=>$list,
            'total'=>$total
        ];
        return $data;

    }


}
