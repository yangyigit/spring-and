<?php
// 这是系统自动生成的公共文件
if (!function_exists('jsonReturn')) {

    /**
     * 输出函数
     * @param array $data 输出的数据
     * @param string $info 提示信息
     * @param int $status 状态，请求成功200，请求失败400
     * @return \think\response\Json 返回json数据
     */
    function jsonReturn($data, $info = '', $status)
    {
        $res = [
            'status' => $status,
            'info' => $info,
            'data' => $data
        ];

        $header = [
            'Access-Control-Allow-Origin' => '*',
            'Access-Control-Allow-Headers' => '*',
            'Access-Control-Allow-Methods' => '*',
            'Access-Control-Allow-Credentials' => 'false'
        ];

        return json($res, 200, $header);
    }

    /**
     * 获取分类名称
     */

    function getCatName($id)
    {
        if($id){
            $data = \think\facade\Db::name('mall_cate')
                    ->where('id',$id)
                    ->value('title');
        }
        return $data;
    }

}
