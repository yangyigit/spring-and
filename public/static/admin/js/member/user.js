define(["jquery", "easy-admin"], function ($, ea) {

    var init = {
        table_elem: '#currentTable',
        table_render_id: 'currentTableRenderId',
        index_url: 'member.user/index',
        add_url: 'member.user/add',
        edit_url: 'member.user/edit',
        delete_url: 'member.user/delete',
        export_url: 'member.user/export',
        modify_url: 'member.user/modify',
    };

    var Controller = {

        index: function () {
            ea.table.render({
                init: init,
                cols: [[
                    {type: 'checkbox'},                    {field: 'id', title: 'id'},                    {field: 'auth_ids', title: '角色权限ID'},                    {field: 'head_img', title: '头像'},                    {field: 'username', title: '用户登录名'},                    {field: 'password', title: '用户登录密码'},                    {field: 'phone', title: '联系手机号'},                    {field: 'remark', title: '备注说明', templet: ea.table.text},                    {field: 'login_num', title: '登录次数'},                    {field: 'sort', title: '排序', edit: 'text'},                    {field: 'status', title: '状态(0:禁用,1:启用,)', templet: ea.table.switch},                    {field: 'create_time', title: '创建时间'},                    {width: 250, title: '操作', templet: ea.table.tool},
                ]],
            });

            ea.listen();
        },
        add: function () {
            ea.listen();
        },
        edit: function () {
            ea.listen();
        },
    };
    return Controller;
});