define(["jquery", "easy-admin"], function ($, ea) {

    var init = {
        table_elem: '#currentTable',
        table_render_id: 'currentTableRenderId',
        index_url: 'mall.card/index',
        add_url: 'mall.card/add',
        edit_url: 'mall.card/edit',
        delete_url: 'mall.card/delete',
        export_url: 'mall.card/export',
        modify_url: 'mall.card/modify',
    };

    var Controller = {

        index: function () {
            ea.table.render({
                init: init,
                toolbar: ['refresh',
                    [{
                        text: '添加',
                        url: init.add_url,
                        method: 'open',
                        auth: 'add',
                        class: 'layui-btn layui-btn-normal layui-btn-sm',
                        icon: 'fa fa-plus ',
                        extend: 'data-full="true"',
                    }],
                    'delete', 'export'],
                cols: [[
                    {type: 'checkbox'},
                    {field: 'id', title: 'id'},
                    {field: 'cate_id', title: '分类ID'},
                    {field: 'title', title: '卡项名称'},
                    {field: 'type', search: 'select', selectList: {"1":"次卡","2":"时卡","3":"通卡"}, title: '卡类型'},
                    {field: 'sort', title: '排序', edit: 'text'},
                    {field: 'status', title: '状态', templet: ea.table.switch},
                    {field: 'recommend', title: '推荐', templet: ea.table.switch},
                    {field: 'create_time', title: '有效期'},
                    {
                        width: 250,
                        title: '操作',
                        templet: ea.table.tool,
                        operat: [
                            [{
                                text: '编辑',
                                url: init.edit_url,
                                method: 'open',
                                auth: 'edit',
                                class: 'layui-btn layui-btn-xs layui-btn-success',
                                extend: 'data-full="true"',
                            }],
                            'delete']
                    },
                ]],
            });

            ea.listen();
        },
        add: function () {
            ea.listen();
        },
        edit: function () {
            ea.listen();
        },
    };
    return Controller;
});
