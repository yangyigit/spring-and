define(["jquery", "easy-admin"], function ($, ea) {

    var init = {
        table_elem: '#currentTable',
        table_render_id: 'currentTableRenderId',
        index_url: 'mall.service/index',
        add_url: 'mall.service/add',
        edit_url: 'mall.service/edit',
        delete_url: 'mall.service/delete',
        export_url: 'mall.service/export',
        modify_url: 'mall.service/modify',
    };

    var Controller = {

        index: function () {
            ea.table.render({
                init: init,
                toolbar: ['refresh',
                    [{
                        text: '添加',
                        url: init.add_url,
                        method: 'open',
                        auth: 'add',
                        class: 'layui-btn layui-btn-normal layui-btn-sm',
                        icon: 'fa fa-plus ',
                        extend: 'data-full="true"',
                    }],
                    'delete', 'export'],
                cols: [[
                    {type: 'checkbox'},
                    {field: 'id', title: 'id'},
                    {field: 'title', title: '服务名称'},
                    {field: 'market_price', title: '市场价'},
                    {field: 'discount_price', title: '折扣价'},
                    {field: 'sales', title: '销量'},
                    {field: 'sort', title: '排序', edit: 'text'},
                    {field: 'status', title: '状态', templet: ea.table.switch},
                    {field: 'recommend', title: '推荐', templet: ea.table.switch},
                    {
                        width: 250,
                        title: '操作',
                        templet: ea.table.tool,
                        operat: [
                            [{
                                text: '编辑',
                                url: init.edit_url,
                                method: 'open',
                                auth: 'edit',
                                class: 'layui-btn layui-btn-xs layui-btn-success',
                                extend: 'data-full="true"',
                            }],
                            'delete']
                    },
                ]],
            });

            ea.listen();
        },
        add: function () {
            ea.listen();
        },
        edit: function () {
            ea.listen();
        },
    };
    return Controller;
});
