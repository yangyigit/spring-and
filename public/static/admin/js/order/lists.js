define(["jquery", "easy-admin"], function ($, ea) {

    var init = {
        table_elem: '#currentTable',
        table_render_id: 'currentTableRenderId',
        index_url: 'order.lists/index',
        add_url: 'order.lists/add',
        edit_url: 'order.lists/edit',
        delete_url: 'order.lists/delete',
        export_url: 'order.lists/export',
        modify_url: 'order.lists/modify',
    };

    var Controller = {

        index: function () {
            ea.table.render({
                init: init,
                cols: [[
                    {type: 'checkbox'},                    {field: 'id', title: 'id'},                    {field: 'goods_id', title: '收费项目'},                    {field: 'user_id', title: '购买人Id'},                    {field: 'create_time', title: '订单创建时间'},                    {field: 'pay_time', title: '支付时间'},                    {field: 'shipping_time', title: '发货时间'},                    {field: 'order_status', search: 'select', selectList: {"0":"未确认","2":"已确认","3":"已取消"}, title: '订单状态'},                    {field: 'shipping_status', search: 'select', selectList: {"0":"未发货","2":"已发货"}, title: '发货状态'},                    {field: 'pay_status', search: 'select', selectList: {"0":"未支付","2":"已支付","3":"已退款"}, title: '支付状态'},                    {field: 'total_amount', title: '订单金额'},                    {field: 'order_amount', title: '应付金额'},                    {field: 'order_prom_amount', title: '优惠金额'},                    {field: 'prom_type', search: 'select', selectList: {"1":"服务","2":"充值","3":"产品"}, title: '订单类型'},                    {field: 'appointment_id', title: '关联预约'},                    {width: 250, title: '操作', templet: ea.table.tool},
                ]],
            });

            ea.listen();
        },
        add: function () {
            ea.listen();
        },
        edit: function () {
            ea.listen();
        },
    };
    return Controller;
});