define(["jquery", "easy-admin"], function ($, ea) {

    var init = {
        table_elem: '#currentTable',
        table_render_id: 'currentTableRenderId',
        index_url: 'appointment.record/index',
        add_url: 'appointment.record/add',
        edit_url: 'appointment.record/edit',
        delete_url: 'appointment.record/delete',
        export_url: 'appointment.record/export',
        modify_url: 'appointment.record/modify',
    };

    var Controller = {

        index: function () {
            ea.table.render({
                init: init,
                cols: [[
                    {type: 'checkbox'},                    {field: 'id', title: 'id'},                    {field: 'appointment_code', title: '预约码'},                    {field: 'goods_id', title: '预约项目'},                    {field: 'user_id', title: '预约人'},                    {field: 'create_time', title: '预约时间'},                    {width: 250, title: '操作', templet: ea.table.tool},
                ]],
            });

            ea.listen();
        },
        add: function () {
            ea.listen();
        },
        edit: function () {
            ea.listen();
        },
    };
    return Controller;
});