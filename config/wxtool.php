<?php
// +----------------------------------------------------------------------
// | 多语言设置
// +----------------------------------------------------------------------

use think\facade\Env;

return [
    // appid
    'wx_appid' => Env::get('weixin.appid', ''),
    // secret
    'wx_secret' => Env::get('weixin.secret', ''),
    // 获取session_key 地址
    'wx_code_url' => Env::get('weixin.wx_code_url', 'https://api.weixin.qq.com/sns/jscode2session?appid=%s&secret=%s&js_code=%s&grant_type=authorization_code'),
];
