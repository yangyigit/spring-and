/*
 Navicat Premium Data Transfer

 Source Server         : 本地
 Source Server Type    : MySQL
 Source Server Version : 50726
 Source Host           : localhost:3306
 Source Schema         : spring-and

 Target Server Type    : MySQL
 Target Server Version : 50726
 File Encoding         : 65001

 Date: 04/01/2022 19:23:20
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for sa_appointment_record
-- ----------------------------
DROP TABLE IF EXISTS `sa_appointment_record`;
CREATE TABLE `sa_appointment_record`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `appointment_code` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '预约码',
  `goods_id` bigint(11) NOT NULL COMMENT '预约项目',
  `user_id` int(10) UNSIGNED NULL DEFAULT 0 COMMENT '预约人',
  `create_time` int(11) NULL DEFAULT NULL COMMENT '预约时间',
  `delete_time` int(11) NULL DEFAULT NULL COMMENT '删除时间',
  `appointment_name` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '预约人姓名',
  `status` tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '状态(0:未确认,2:已确认,3:已取消)',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '预约记录' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sa_appointment_record
-- ----------------------------

-- ----------------------------
-- Table structure for sa_mall_card
-- ----------------------------
DROP TABLE IF EXISTS `sa_mall_card`;
CREATE TABLE `sa_mall_card`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `cate_id` int(11) NULL DEFAULT NULL COMMENT '分类ID',
  `title` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '卡项名称',
  `logo` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '卡项logo',
  `describe` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '卡项描述',
  `market_price` decimal(10, 2) NULL DEFAULT NULL COMMENT '市场价',
  `discount_price` decimal(10, 2) NULL DEFAULT NULL COMMENT '折扣价',
  `sales` int(11) NULL DEFAULT 0 COMMENT '销量',
  `virtual_sales` int(11) NULL DEFAULT 0 COMMENT '虚拟销量',
  `type` tinyint(1) UNSIGNED NOT NULL DEFAULT 1 COMMENT '卡类型 {select} (1:次卡, 2:时卡, 3:通卡)',
  `sort` int(11) NULL DEFAULT 0 COMMENT '排序',
  `status` tinyint(1) UNSIGNED NULL DEFAULT 1 COMMENT '状态(1:禁用,2:启用)',
  `remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注说明',
  `create_time` int(11) NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` int(11) NULL DEFAULT NULL COMMENT '更新时间',
  `delete_time` int(11) NULL DEFAULT NULL COMMENT '删除时间',
  `start_time` int(11) NULL DEFAULT NULL COMMENT '卡项开始时间',
  `end_time` int(11) NULL DEFAULT NULL COMMENT '卡项结束时间',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `cate_id`(`cate_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '商品列表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sa_mall_card
-- ----------------------------

-- ----------------------------
-- Table structure for sa_mall_cate
-- ----------------------------
DROP TABLE IF EXISTS `sa_mall_cate`;
CREATE TABLE `sa_mall_cate`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `title` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '分类名',
  `image` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '分类图片',
  `sort` int(11) NULL DEFAULT 0 COMMENT '排序',
  `status` tinyint(1) UNSIGNED NULL DEFAULT 1 COMMENT '状态(1:禁用,2:启用)',
  `type` tinyint(1) UNSIGNED NOT NULL DEFAULT 1 COMMENT '分类类型 {radio} (1:服务, 2:卡项, 3:产品)',
  `remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注说明',
  `create_time` int(11) NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` int(11) NULL DEFAULT NULL COMMENT '更新时间',
  `delete_time` int(11) NULL DEFAULT NULL COMMENT '删除时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `title`(`title`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '商品分类' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sa_mall_cate
-- ----------------------------
INSERT INTO `sa_mall_cate` VALUES (1, '洗护', '', 0, 1, 1, '', 1640657617, 1640657617, NULL);
INSERT INTO `sa_mall_cate` VALUES (2, '清洁', '', 0, 1, 1, '', 1640657627, 1640657627, NULL);

-- ----------------------------
-- Table structure for sa_mall_goods
-- ----------------------------
DROP TABLE IF EXISTS `sa_mall_goods`;
CREATE TABLE `sa_mall_goods`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `cate_id` int(11) NULL DEFAULT NULL COMMENT '分类ID',
  `title` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '商品名称',
  `logo` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '商品logo',
  `images` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '商品图片 以 | 做分割符号',
  `describe` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '商品描述',
  `market_price` decimal(10, 2) NULL DEFAULT NULL COMMENT '市场价',
  `discount_price` decimal(10, 2) NULL DEFAULT NULL COMMENT '折扣价',
  `sales` int(11) NULL DEFAULT 0 COMMENT '销量',
  `virtual_sales` int(11) NULL DEFAULT 0 COMMENT '虚拟销量',
  `stock` int(11) NULL DEFAULT 0 COMMENT '库存',
  `total_stock` int(11) NULL DEFAULT 0 COMMENT '总库存',
  `sort` int(11) NULL DEFAULT 0 COMMENT '排序',
  `status` tinyint(1) UNSIGNED NULL DEFAULT 1 COMMENT '状态(1:禁用,2:启用)',
  `remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注说明',
  `create_time` int(11) NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` int(11) NULL DEFAULT NULL COMMENT '更新时间',
  `delete_time` int(11) NULL DEFAULT NULL COMMENT '删除时间',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `cate_id`(`cate_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 10 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '商品列表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sa_mall_goods
-- ----------------------------
INSERT INTO `sa_mall_goods` VALUES (8, 10, '落地-风扇', 'http://admin.host/upload/20200514/a0f7fe9637abd219f7e93ceb2820df9b.jpg', 'http://admin.host/upload/20200514/95496713918290f6315ea3f87efa6bf2.jpg|http://admin.host/upload/20200514/ae29fa9cba4fc02defb7daed41cb2b13.jpg|http://admin.host/upload/20200514/f0a104d88ec7dc6fb42d2f87cbc71b76.jpg|http://admin.host/upload/20200514/3b88be4b1934690e5c1bd6b54b9ab5c8.jpg', '<p>76654757</p>\n\n<p><img alt=\"\" src=\"http://admin.host/upload/20200515/198070421110fa01f2c2ac2f52481647.jpg\" style=\"height:689px; width:790px\" /></p>\n\n<p><img alt=\"\" src=\"http://admin.host/upload/20200515/a07a742c15a78781e79f8a3317006c1d.jpg\" style=\"height:877px; width:790px\" /></p>\n', 599.00, 368.00, 0, 594, 0, 0, 675, 1, '', 1589454309, 1589567016, NULL);
INSERT INTO `sa_mall_goods` VALUES (9, 9, '电脑', 'http://admin.host/upload/20200514/bbf858d469dec2e12a89460110068d3d.jpg', 'http://admin.host/upload/20200514/f0a104d88ec7dc6fb42d2f87cbc71b76.jpg', '<p>477</p>\n', 0.00, 0.00, 0, 0, 115, 320, 0, 1, '', 1589465215, 1589476345, NULL);

-- ----------------------------
-- Table structure for sa_mall_service
-- ----------------------------
DROP TABLE IF EXISTS `sa_mall_service`;
CREATE TABLE `sa_mall_service`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `cate_id` int(11) NULL DEFAULT NULL COMMENT '分类ID',
  `title` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '服务名称',
  `logo` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '服务logo',
  `images` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '服务图片 以 | 做分割符号',
  `describe` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '服务描述',
  `market_price` decimal(10, 2) NOT NULL COMMENT '市场价',
  `discount_price` decimal(10, 2) NULL DEFAULT NULL COMMENT '折扣价',
  `sales` int(11) NULL DEFAULT 0 COMMENT '销量',
  `virtual_sales` int(11) NULL DEFAULT 0 COMMENT '虚拟销量',
  `sort` int(11) NULL DEFAULT 0 COMMENT '排序',
  `status` tinyint(1) UNSIGNED NULL DEFAULT 1 COMMENT '状态(1:禁用,2:启用)',
  `remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注说明',
  `create_time` int(11) NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` int(11) NULL DEFAULT NULL COMMENT '更新时间',
  `delete_time` int(11) NULL DEFAULT NULL COMMENT '删除时间',
  `service_time` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '服务时间',
  `time_span` tinyint(2) UNSIGNED NOT NULL DEFAULT 0 COMMENT '服务时间间隔',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `cate_id`(`cate_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '商品列表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sa_mall_service
-- ----------------------------
INSERT INTO `sa_mall_service` VALUES (1, 2, '头皮卸妆', 'http://www.spring-and.cn/upload/20211229/500452d863b879d6487a319015657e80.jpg', 'http://www.spring-and.cn/upload/20211229/0ca704a764b574d2e2dc4ce4e1d02eaa.jpg|http://www.spring-and.cn/upload/20211229/8f3824585985cc3c004ba19d5d6a7861.jpg', '&lt;p&gt;这是对于碳酸泉头皮卸妆的描述&lt;/p&gt;\n', 389.00, 0.00, 0, 20, 0, 1, '', 1640764796, 1640764796, NULL, '10:00 - 22:00', 6);
INSERT INTO `sa_mall_service` VALUES (2, 1, '基础清洁', 'http://www.spring-and.cn/upload/20211229/500452d863b879d6487a319015657e80.jpg', 'http://www.spring-and.cn/upload/20211229/0ca704a764b574d2e2dc4ce4e1d02eaa.jpg|http://www.spring-and.cn/upload/20211229/8f3824585985cc3c004ba19d5d6a7861.jpg', '&lt;p&gt;这是对于碳酸泉头皮卸妆的描述&lt;/p&gt;\n', 389.00, 0.00, 0, 20, 0, 1, '', 1640764796, 1640764796, NULL, '10:00 - 22:00', 6);

-- ----------------------------
-- Table structure for sa_member_invest
-- ----------------------------
DROP TABLE IF EXISTS `sa_member_invest`;
CREATE TABLE `sa_member_invest`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '备注说明',
  `status` tinyint(1) UNSIGNED NOT NULL DEFAULT 1 COMMENT '状态(0:禁用,1:启用)',
  `create_time` int(11) NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` int(11) NULL DEFAULT NULL COMMENT '更新时间',
  `delete_time` int(11) NULL DEFAULT NULL COMMENT '删除时间',
  `support_recharge_method` tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '支持储值方式 {switch} (0:商家代充,1:商家代充+会员直充)',
  `member_stored_value_mode` tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '会员储值模式 {switch} (0:普通储值模式,1:按档位储值模式)',
  `recharge_amount` int(5) UNSIGNED NULL DEFAULT NULL COMMENT '充值金额',
  `gift_amount` int(5) UNSIGNED NULL DEFAULT NULL COMMENT '赠送金额',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '系统用户表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sa_member_invest
-- ----------------------------
INSERT INTO `sa_member_invest` VALUES (1, '', 1, 1639819814, 1639819832, NULL, 0, 0, NULL, NULL);

-- ----------------------------
-- Table structure for sa_member_invest_record
-- ----------------------------
DROP TABLE IF EXISTS `sa_member_invest_record`;
CREATE TABLE `sa_member_invest_record`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '备注说明',
  `create_time` int(11) NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` int(11) NULL DEFAULT NULL COMMENT '更新时间',
  `delete_time` int(11) NULL DEFAULT NULL COMMENT '删除时间',
  `recharge_amount` int(5) UNSIGNED NULL DEFAULT NULL COMMENT '充值金额',
  `gift_amount` int(5) UNSIGNED NULL DEFAULT NULL COMMENT '赠送金额',
  `user_id` int(10) UNSIGNED NULL DEFAULT 0 COMMENT '充值人id',
  `admin_id` int(10) UNSIGNED NULL DEFAULT 0 COMMENT '操作人id',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '系统用户表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sa_member_invest_record
-- ----------------------------
INSERT INTO `sa_member_invest_record` VALUES (1, '', 1639819814, 1639819832, NULL, NULL, NULL, 0, 0);

-- ----------------------------
-- Table structure for sa_member_user
-- ----------------------------
DROP TABLE IF EXISTS `sa_member_user`;
CREATE TABLE `sa_member_user`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `auth_ids` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '角色权限ID',
  `head_img` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '头像',
  `username` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '用户登录名',
  `password` char(40) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '用户登录密码',
  `phone` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '联系手机号',
  `remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '备注说明',
  `login_num` bigint(20) UNSIGNED NULL DEFAULT 0 COMMENT '登录次数',
  `sort` int(11) NULL DEFAULT 0 COMMENT '排序',
  `status` tinyint(1) UNSIGNED NOT NULL DEFAULT 1 COMMENT '状态(0:禁用,1:启用)',
  `user_money` decimal(10, 2) NULL DEFAULT NULL COMMENT '账户金额',
  `create_time` int(11) NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` int(11) NULL DEFAULT NULL COMMENT '更新时间',
  `delete_time` int(11) NULL DEFAULT NULL COMMENT '删除时间',
  `oauth` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '第三方来源 wx weibo alipay',
  `openid` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '第三方唯一标示',
  `unionid` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `username`(`username`) USING BTREE,
  UNIQUE INDEX `openid`(`openid`) USING BTREE,
  UNIQUE INDEX `unionid`(`unionid`) USING BTREE,
  INDEX `phone`(`phone`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '系统用户表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sa_member_user
-- ----------------------------
INSERT INTO `sa_member_user` VALUES (1, NULL, NULL, '', '', NULL, '', 28, 0, 1, NULL, NULL, NULL, NULL, '', 'oEV_d4sR6-LiAhVb0TcLC-RqxjKY', NULL);

-- ----------------------------
-- Table structure for sa_order_lists
-- ----------------------------
DROP TABLE IF EXISTS `sa_order_lists`;
CREATE TABLE `sa_order_lists`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `goods_id` bigint(11) NOT NULL COMMENT '收费项目',
  `user_id` int(10) UNSIGNED NULL DEFAULT 0 COMMENT '购买人Id',
  `create_time` int(11) NOT NULL COMMENT '订单创建时间',
  `pay_time` int(11) NULL DEFAULT NULL COMMENT '支付时间',
  `shipping_time` int(11) NULL DEFAULT NULL COMMENT '发货时间',
  `order_status` tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '订单状态 {select} (0:未确认,2:已确认,3:已取消)',
  `shipping_status` tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '发货状态 {select} (0:未发货,2:已发货)',
  `pay_status` tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '支付状态 {select} (0:未支付,2:已支付,3:已退款)',
  `total_amount` decimal(10, 2) NOT NULL COMMENT '订单金额',
  `order_amount` decimal(10, 2) NOT NULL COMMENT '应付金额',
  `order_prom_amount` decimal(10, 2) NULL DEFAULT NULL COMMENT '优惠金额',
  `prom_type` tinyint(4) UNSIGNED NOT NULL DEFAULT 0 COMMENT '订单类型 {radio} (1:服务,2:充值,3:产品)',
  `appointment_id` int(20) UNSIGNED NOT NULL COMMENT '关联预约',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '预约记录' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sa_order_lists
-- ----------------------------

-- ----------------------------
-- Table structure for sa_system_admin
-- ----------------------------
DROP TABLE IF EXISTS `sa_system_admin`;
CREATE TABLE `sa_system_admin`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `auth_ids` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '角色权限ID',
  `head_img` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '头像',
  `username` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '用户登录名',
  `password` char(40) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '用户登录密码',
  `phone` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '联系手机号',
  `remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '备注说明',
  `login_num` bigint(20) UNSIGNED NULL DEFAULT 0 COMMENT '登录次数',
  `sort` int(11) NULL DEFAULT 0 COMMENT '排序',
  `status` tinyint(1) UNSIGNED NOT NULL DEFAULT 1 COMMENT '状态(0:禁用,1:启用,)',
  `create_time` int(11) NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` int(11) NULL DEFAULT NULL COMMENT '更新时间',
  `delete_time` int(11) NULL DEFAULT NULL COMMENT '删除时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `username`(`username`) USING BTREE,
  INDEX `phone`(`phone`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '系统用户表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sa_system_admin
-- ----------------------------
INSERT INTO `sa_system_admin` VALUES (1, NULL, '/static/admin/images/head.jpg', 'admin', 'ed696eb5bba1f7460585cc6975e6cf9bf24903dd', NULL, '', 10, 0, 1, 1639819814, 1640773092, NULL);

-- ----------------------------
-- Table structure for sa_system_auth
-- ----------------------------
DROP TABLE IF EXISTS `sa_system_auth`;
CREATE TABLE `sa_system_auth`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `title` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '权限名称',
  `sort` int(11) NULL DEFAULT 0 COMMENT '排序',
  `status` tinyint(1) UNSIGNED NULL DEFAULT 1 COMMENT '状态(1:禁用,2:启用)',
  `remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注说明',
  `create_time` int(11) NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` int(11) NULL DEFAULT NULL COMMENT '更新时间',
  `delete_time` int(11) NULL DEFAULT NULL COMMENT '删除时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `title`(`title`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '系统权限表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sa_system_auth
-- ----------------------------
INSERT INTO `sa_system_auth` VALUES (1, '管理员', 1, 1, '测试管理员', 1588921753, 1589614331, NULL);
INSERT INTO `sa_system_auth` VALUES (6, '游客权限', 0, 1, '', 1588227513, 1589591751, 1589591751);

-- ----------------------------
-- Table structure for sa_system_auth_node
-- ----------------------------
DROP TABLE IF EXISTS `sa_system_auth_node`;
CREATE TABLE `sa_system_auth_node`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `auth_id` bigint(20) UNSIGNED NULL DEFAULT NULL COMMENT '角色ID',
  `node_id` bigint(20) NULL DEFAULT NULL COMMENT '节点ID',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `index_system_auth_auth`(`auth_id`) USING BTREE,
  INDEX `index_system_auth_node`(`node_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 17 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '角色与节点关系表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sa_system_auth_node
-- ----------------------------
INSERT INTO `sa_system_auth_node` VALUES (1, 6, 1);
INSERT INTO `sa_system_auth_node` VALUES (2, 6, 2);
INSERT INTO `sa_system_auth_node` VALUES (3, 6, 9);
INSERT INTO `sa_system_auth_node` VALUES (4, 6, 12);
INSERT INTO `sa_system_auth_node` VALUES (5, 6, 18);
INSERT INTO `sa_system_auth_node` VALUES (6, 6, 19);
INSERT INTO `sa_system_auth_node` VALUES (7, 6, 21);
INSERT INTO `sa_system_auth_node` VALUES (8, 6, 22);
INSERT INTO `sa_system_auth_node` VALUES (9, 6, 29);
INSERT INTO `sa_system_auth_node` VALUES (10, 6, 30);
INSERT INTO `sa_system_auth_node` VALUES (11, 6, 38);
INSERT INTO `sa_system_auth_node` VALUES (12, 6, 39);
INSERT INTO `sa_system_auth_node` VALUES (13, 6, 45);
INSERT INTO `sa_system_auth_node` VALUES (14, 6, 46);
INSERT INTO `sa_system_auth_node` VALUES (15, 6, 52);
INSERT INTO `sa_system_auth_node` VALUES (16, 6, 53);

-- ----------------------------
-- Table structure for sa_system_config
-- ----------------------------
DROP TABLE IF EXISTS `sa_system_config`;
CREATE TABLE `sa_system_config`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '变量名',
  `group` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '分组',
  `value` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '变量值',
  `remark` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '备注信息',
  `sort` int(10) NULL DEFAULT 0,
  `create_time` int(11) NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` int(11) NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `name`(`name`) USING BTREE,
  INDEX `group`(`group`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 88 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '系统配置表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sa_system_config
-- ----------------------------
INSERT INTO `sa_system_config` VALUES (41, 'alisms_access_key_id', 'sms', '填你的', '阿里大于公钥', 0, NULL, NULL);
INSERT INTO `sa_system_config` VALUES (42, 'alisms_access_key_secret', 'sms', '填你的', '阿里大鱼私钥', 0, NULL, NULL);
INSERT INTO `sa_system_config` VALUES (55, 'upload_type', 'upload', 'local', '当前上传方式 （local,alioss,qnoss,txoss）', 0, NULL, NULL);
INSERT INTO `sa_system_config` VALUES (56, 'upload_allow_ext', 'upload', 'doc,gif,ico,icon,jpg,mp3,mp4,p12,pem,png,rar,jpeg', '允许上传的文件类型', 0, NULL, NULL);
INSERT INTO `sa_system_config` VALUES (57, 'upload_allow_size', 'upload', '1024000', '允许上传的大小', 0, NULL, NULL);
INSERT INTO `sa_system_config` VALUES (58, 'upload_allow_mime', 'upload', 'image/gif,image/jpeg,video/x-msvideo,text/plain,image/png', '允许上传的文件mime', 0, NULL, NULL);
INSERT INTO `sa_system_config` VALUES (59, 'upload_allow_type', 'upload', 'local,alioss,qnoss,txcos', '可用的上传文件方式', 0, NULL, NULL);
INSERT INTO `sa_system_config` VALUES (60, 'alioss_access_key_id', 'upload', '填你的', '阿里云oss公钥', 0, NULL, NULL);
INSERT INTO `sa_system_config` VALUES (61, 'alioss_access_key_secret', 'upload', '填你的', '阿里云oss私钥', 0, NULL, NULL);
INSERT INTO `sa_system_config` VALUES (62, 'alioss_endpoint', 'upload', '填你的', '阿里云oss数据中心', 0, NULL, NULL);
INSERT INTO `sa_system_config` VALUES (63, 'alioss_bucket', 'upload', '填你的', '阿里云oss空间名称', 0, NULL, NULL);
INSERT INTO `sa_system_config` VALUES (64, 'alioss_domain', 'upload', '填你的', '阿里云oss访问域名', 0, NULL, NULL);
INSERT INTO `sa_system_config` VALUES (65, 'logo_title', 'site', '春和碳酸泉', 'LOGO标题', 0, NULL, NULL);
INSERT INTO `sa_system_config` VALUES (66, 'logo_image', 'site', '/favicon.ico', 'logo图片', 0, NULL, NULL);
INSERT INTO `sa_system_config` VALUES (68, 'site_name', 'site', '春和碳酸泉头皮护理济南店', '站点名称', 0, NULL, NULL);
INSERT INTO `sa_system_config` VALUES (69, 'site_ico', 'site', '填你的', '浏览器图标', 0, NULL, NULL);
INSERT INTO `sa_system_config` VALUES (70, 'site_copyright', 'site', '填你的', '版权信息', 0, NULL, NULL);
INSERT INTO `sa_system_config` VALUES (71, 'site_beian', 'site', '填你的', '备案信息', 0, NULL, NULL);
INSERT INTO `sa_system_config` VALUES (72, 'site_version', 'site', '2.0.0', '版本信息', 0, NULL, NULL);
INSERT INTO `sa_system_config` VALUES (75, 'sms_type', 'sms', 'alisms', '短信类型', 0, NULL, NULL);
INSERT INTO `sa_system_config` VALUES (76, 'miniapp_appid', 'wechat', '填你的', '小程序公钥', 0, NULL, NULL);
INSERT INTO `sa_system_config` VALUES (77, 'miniapp_appsecret', 'wechat', '填你的', '小程序私钥', 0, NULL, NULL);
INSERT INTO `sa_system_config` VALUES (78, 'web_appid', 'wechat', '填你的', '公众号公钥', 0, NULL, NULL);
INSERT INTO `sa_system_config` VALUES (79, 'web_appsecret', 'wechat', '填你的', '公众号私钥', 0, NULL, NULL);
INSERT INTO `sa_system_config` VALUES (80, 'txcos_secret_id', 'upload', '填你的', '腾讯云cos密钥', 0, NULL, NULL);
INSERT INTO `sa_system_config` VALUES (81, 'txcos_secret_key', 'upload', '填你的', '腾讯云cos私钥', 0, NULL, NULL);
INSERT INTO `sa_system_config` VALUES (82, 'txcos_region', 'upload', '填你的', '存储桶地域', 0, NULL, NULL);
INSERT INTO `sa_system_config` VALUES (83, 'tecos_bucket', 'upload', '填你的', '存储桶名称', 0, NULL, NULL);
INSERT INTO `sa_system_config` VALUES (84, 'qnoss_access_key', 'upload', '填你的', '访问密钥', 0, NULL, NULL);
INSERT INTO `sa_system_config` VALUES (85, 'qnoss_secret_key', 'upload', '填你的', '安全密钥', 0, NULL, NULL);
INSERT INTO `sa_system_config` VALUES (86, 'qnoss_bucket', 'upload', '填你的', '存储空间', 0, NULL, NULL);
INSERT INTO `sa_system_config` VALUES (87, 'qnoss_domain', 'upload', '填你的', '访问域名', 0, NULL, NULL);

-- ----------------------------
-- Table structure for sa_system_log_202112
-- ----------------------------
DROP TABLE IF EXISTS `sa_system_log_202112`;
CREATE TABLE `sa_system_log_202112`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `admin_id` int(10) UNSIGNED NULL DEFAULT 0 COMMENT '管理员ID',
  `url` varchar(1500) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '操作页面',
  `method` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '请求方法',
  `title` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '日志标题',
  `content` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '内容',
  `ip` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT 'IP',
  `useragent` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT 'User-Agent',
  `create_time` int(10) NULL DEFAULT NULL COMMENT '操作时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 680 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '后台操作日志表 - 202112' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sa_system_log_202112
-- ----------------------------
INSERT INTO `sa_system_log_202112` VALUES (630, NULL, '/admin/login/index.html', 'post', '', '{\"username\":\"admin\",\"password\":\"***********\",\"captcha\":\"jcfx\",\"keep_login\":\"1\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36', 1639819832);
INSERT INTO `sa_system_log_202112` VALUES (631, 1, '/admin/system.node/refreshNode?force=0', 'post', '', '{\"force\":\"0\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36', 1639820464);
INSERT INTO `sa_system_log_202112` VALUES (632, 1, '/admin/system.menu/add', 'post', '', '{\"pid\":\"0\",\"title\":\"会员管理\",\"href\":\"\",\"icon\":\"fa fa-user\",\"target\":\"_self\",\"sort\":\"3\",\"remark\":\"\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36', 1639820555);
INSERT INTO `sa_system_log_202112` VALUES (633, 1, '/admin/system.menu/edit?id=254', 'post', '', '{\"id\":\"254\",\"pid\":\"0\",\"title\":\"会员管理\",\"href\":\"\",\"icon\":\"fa fa-user\",\"target\":\"_self\",\"sort\":\"0\",\"remark\":\"\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36', 1639820598);
INSERT INTO `sa_system_log_202112` VALUES (634, 1, '/admin/system.menu/add?id=254', 'post', '', '{\"id\":\"254\",\"pid\":\"254\",\"title\":\"会员列表\",\"href\":\"member.user\\/index\",\"icon\":\"fa fa-users\",\"target\":\"_self\",\"sort\":\"0\",\"remark\":\"\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36', 1639820674);
INSERT INTO `sa_system_log_202112` VALUES (635, NULL, '/admin/login/index.html', 'post', '', '{\"username\":\"admin\",\"password\":\"***********\",\"captcha\":\"p4c7\",\"keep_login\":\"0\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36', 1639922330);
INSERT INTO `sa_system_log_202112` VALUES (636, 1, '/admin/system.node/refreshNode?force=0', 'post', '', '{\"force\":\"0\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36', 1639923400);
INSERT INTO `sa_system_log_202112` VALUES (637, 1, '/admin/system.node/refreshNode?force=0', 'post', '', '{\"force\":\"0\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36', 1639923405);
INSERT INTO `sa_system_log_202112` VALUES (638, 1, '/admin/system.node/refreshNode?force=0', 'post', '', '{\"force\":\"0\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36', 1639923482);
INSERT INTO `sa_system_log_202112` VALUES (639, 1, '/admin/system.node/modify', 'post', '', '{\"id\":\"76\",\"field\":\"title\",\"value\":\"预约管理\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36', 1639923494);
INSERT INTO `sa_system_log_202112` VALUES (640, 1, '/admin/system.node/modify', 'post', '', '{\"id\":\"69\",\"field\":\"title\",\"value\":\"会员管理\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36', 1639923502);
INSERT INTO `sa_system_log_202112` VALUES (641, 1, '/admin/system.node/modify', 'post', '', '{\"id\":\"76\",\"field\":\"title\",\"value\":\"预约记录\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36', 1639923592);
INSERT INTO `sa_system_log_202112` VALUES (642, 1, '/admin/system.node/modify', 'post', '', '{\"id\":\"69\",\"field\":\"title\",\"value\":\"会员列表\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36', 1639923601);
INSERT INTO `sa_system_log_202112` VALUES (643, 1, '/admin/system.menu/add', 'post', '', '{\"pid\":\"0\",\"title\":\"预约管理\",\"href\":\"\",\"icon\":\"fa fa-calendar-check-o\",\"target\":\"_self\",\"sort\":\"0\",\"remark\":\"\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36', 1639923793);
INSERT INTO `sa_system_log_202112` VALUES (644, 1, '/admin/system.menu/edit?id=256', 'post', '', '{\"id\":\"256\",\"pid\":\"0\",\"title\":\"预约管理\",\"href\":\"\",\"icon\":\"fa fa-calendar-check-o\",\"target\":\"_self\",\"sort\":\"0\",\"remark\":\"\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36', 1639923808);
INSERT INTO `sa_system_log_202112` VALUES (645, 1, '/admin/system.menu/add?id=256', 'post', '', '{\"id\":\"256\",\"pid\":\"256\",\"title\":\"预约记录\",\"href\":\"appointment.record\\/index\",\"icon\":\"fa fa-calendar-check-o\",\"target\":\"_self\",\"sort\":\"0\",\"remark\":\"\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36', 1639923867);
INSERT INTO `sa_system_log_202112` VALUES (646, 1, '/admin/system.menu/add?id=256', 'post', '', '{\"id\":\"256\",\"pid\":\"256\",\"title\":\"预约记录\",\"href\":\"appointment.record\\/index\",\"icon\":\"fa fa-calendar-check-o\",\"target\":\"_self\",\"sort\":\"0\",\"remark\":\"\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36', 1639923874);
INSERT INTO `sa_system_log_202112` VALUES (647, 1, '/admin/system.menu/add?id=256', 'post', '', '{\"id\":\"256\",\"pid\":\"256\",\"title\":\"预约记录\",\"href\":\"appointment.record\\/index\",\"icon\":\"fa fa-calendar-check-o\",\"target\":\"_self\",\"sort\":\"0\",\"remark\":\"\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36', 1639923890);
INSERT INTO `sa_system_log_202112` VALUES (648, 1, '/admin/system.menu/edit?id=256', 'post', '', '{\"id\":\"256\",\"pid\":\"0\",\"title\":\"预约管理\",\"href\":\"\",\"icon\":\"fa fa-calendar\",\"target\":\"_self\",\"sort\":\"0\",\"remark\":\"\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36', 1639923905);
INSERT INTO `sa_system_log_202112` VALUES (649, 1, '/admin/system.node/refreshNode?force=1', 'post', '', '{\"force\":\"1\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36', 1639924286);
INSERT INTO `sa_system_log_202112` VALUES (650, NULL, '/admin/login/index.html', 'post', '', '{\"username\":\"admin\",\"password\":\"***********\",\"captcha\":\"dcth\",\"keep_login\":\"0\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36', 1639966110);
INSERT INTO `sa_system_log_202112` VALUES (651, 1, '/admin/system.node/refreshNode?force=0', 'post', '', '{\"force\":\"0\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36', 1639967914);
INSERT INTO `sa_system_log_202112` VALUES (652, 1, '/admin/system.node/refreshNode?force=0', 'post', '', '{\"force\":\"0\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36', 1639967918);
INSERT INTO `sa_system_log_202112` VALUES (653, 1, '/admin/system.menu/add', 'post', '', '{\"pid\":\"0\",\"title\":\"订单管理\",\"href\":\"\",\"icon\":\"fa fa-shopping-cart\",\"target\":\"_self\",\"sort\":\"0\",\"remark\":\"\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36', 1639967962);
INSERT INTO `sa_system_log_202112` VALUES (654, 1, '/admin/system.menu/add?id=258', 'post', '', '{\"id\":\"258\",\"pid\":\"258\",\"title\":\"订单列表\",\"href\":\"order.lists\\/index\",\"icon\":\"fa fa-shopping-bag\",\"target\":\"_self\",\"sort\":\"0\",\"remark\":\"\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36', 1639967985);
INSERT INTO `sa_system_log_202112` VALUES (655, NULL, '/admin/login/index.html', 'post', '', '{\"username\":\"admin\",\"password\":\"***********\",\"captcha\":\"trtc\",\"keep_login\":\"1\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36', 1640485537);
INSERT INTO `sa_system_log_202112` VALUES (656, NULL, '/admin/login/index.html', 'post', '', '{\"username\":\"admin\",\"password\":\"***********\",\"captcha\":\"bepy\",\"keep_login\":\"1\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36', 1640578169);
INSERT INTO `sa_system_log_202112` VALUES (657, 1, '/admin/system.config/save', 'post', '', '{\"site_name\":\"春和碳酸泉头皮护理济南店\",\"site_ico\":\"填你的\",\"file\":\"\",\"site_version\":\"2.0.0\",\"site_beian\":\"填你的\",\"site_copyright\":\"填你的\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36', 1640578542);
INSERT INTO `sa_system_log_202112` VALUES (658, 1, '/admin/system.config/save', 'post', '', '{\"site_name\":\"春和碳酸泉头皮护理济南店\",\"site_ico\":\"填你的\",\"file\":\"\",\"site_version\":\"2.0.0\",\"site_beian\":\"填你的\",\"site_copyright\":\"填你的\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36', 1640578563);
INSERT INTO `sa_system_log_202112` VALUES (659, 1, '/admin/system.config/save', 'post', '', '{\"logo_title\":\"春和碳酸泉\",\"logo_image\":\"\\/favicon.ico\",\"file\":\"\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36', 1640578665);
INSERT INTO `sa_system_log_202112` VALUES (660, NULL, '/admin/login/index.html', 'post', '', '{\"username\":\"admin\",\"password\":\"***********\",\"captcha\":\"cnpg\",\"keep_login\":\"0\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36', 1640599749);
INSERT INTO `sa_system_log_202112` VALUES (661, 1, '/admin/mall.cate/delete?id=9', 'post', '', '{\"id\":\"9\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36', 1640600000);
INSERT INTO `sa_system_log_202112` VALUES (662, 1, '/admin/mall.cate/delete?id=9', 'post', '', '{\"id\":\"9\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36', 1640600009);
INSERT INTO `sa_system_log_202112` VALUES (663, 1, '/admin/mall.cate/delete?id=9', 'post', '', '{\"id\":\"9\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36', 1640600014);
INSERT INTO `sa_system_log_202112` VALUES (664, 1, '/admin/system.node/refreshNode?force=0', 'post', '', '{\"force\":\"0\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36', 1640600674);
INSERT INTO `sa_system_log_202112` VALUES (665, 1, '/admin/system.menu/add?id=249', 'post', '', '{\"id\":\"249\",\"pid\":\"249\",\"title\":\"服务管理\",\"href\":\"mall.service\\/index\",\"icon\":\"fa fa-hourglass\",\"target\":\"_self\",\"sort\":\"0\",\"remark\":\"\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36', 1640600827);
INSERT INTO `sa_system_log_202112` VALUES (666, NULL, '/admin/login/index.html', 'post', '', '{\"username\":\"admin\",\"password\":\"***********\",\"captcha\":\"pswc\",\"keep_login\":\"1\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36', 1640654454);
INSERT INTO `sa_system_log_202112` VALUES (667, 1, '/admin/system.node/refreshNode?force=0', 'post', '', '{\"force\":\"0\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36', 1640655750);
INSERT INTO `sa_system_log_202112` VALUES (668, 1, '/admin/system.menu/add?id=249', 'post', '', '{\"id\":\"249\",\"pid\":\"249\",\"title\":\"卡项管理\",\"href\":\"mall.card\\/index\",\"icon\":\"fa fa-credit-card-alt\",\"target\":\"_self\",\"sort\":\"0\",\"remark\":\"\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36', 1640655783);
INSERT INTO `sa_system_log_202112` VALUES (669, 1, '/admin/mall.cate/add', 'post', '', '{\"title\":\"洗护\",\"image\":\"\",\"file\":\"\",\"sort\":\"0\",\"remark\":\"\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36', 1640657600);
INSERT INTO `sa_system_log_202112` VALUES (670, 1, '/admin/mall.cate/add', 'post', '', '{\"title\":\"洗护\",\"image\":\"\",\"file\":\"\",\"sort\":\"0\",\"remark\":\"\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36', 1640657617);
INSERT INTO `sa_system_log_202112` VALUES (671, 1, '/admin/mall.cate/add', 'post', '', '{\"title\":\"清洁\",\"image\":\"\",\"file\":\"\",\"sort\":\"0\",\"remark\":\"\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36', 1640657627);
INSERT INTO `sa_system_log_202112` VALUES (672, NULL, '/admin/login/index.html', 'post', '', '{\"username\":\"admin\",\"password\":\"***********\",\"captcha\":\"nnrf\",\"keep_login\":\"1\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36', 1640759818);
INSERT INTO `sa_system_log_202112` VALUES (673, 1, '/admin/mall.service/add', 'post', '', '{\"cate_id\":\"1\",\"title\":\"123123\",\"logo\":\"http:\\/\\/admin.host\\/upload\\/20200514\\/e1c6c9ef6a4b98b8f7d95a1a0191a2df.jpg\",\"file\":\"\",\"images\":\"http:\\/\\/admin.host\\/upload\\/20200514\\/98fc09b0c4ad4d793a6f04bef79a0edc.jpg|http:\\/\\/admin.host\\/upload\\/20200514\\/e1c6c9ef6a4b98b8f7d95a1a0191a2df.jpg|http:\\/\\/admin.host\\/upload\\/20200514\\/577c65f101639f53dbbc9e7aa346f81c.jpg\",\"describe\":\"\",\"time_span\":\"3\",\"service_time\":\"10:00 - 22:00\",\"market_price\":\"10.00\",\"discount_price\":\"5\",\"virtual_sales\":\"10\",\"sort\":\"36\",\"remark\":\"232323\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36', 1640764345);
INSERT INTO `sa_system_log_202112` VALUES (674, 1, '/admin/ajax/upload', 'post', '', '[]', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36', 1640764734);
INSERT INTO `sa_system_log_202112` VALUES (675, 1, '/admin/ajax/upload', 'post', '', '[]', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36', 1640764746);
INSERT INTO `sa_system_log_202112` VALUES (676, 1, '/admin/ajax/upload', 'post', '', '[]', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36', 1640764746);
INSERT INTO `sa_system_log_202112` VALUES (677, 1, '/admin/mall.service/add', 'post', '', '{\"cate_id\":\"2\",\"title\":\"头皮卸妆\",\"logo\":\"http:\\/\\/www.spring-and.cn\\/upload\\/20211229\\/500452d863b879d6487a319015657e80.jpg\",\"file\":\"\",\"images\":\"http:\\/\\/www.spring-and.cn\\/upload\\/20211229\\/0ca704a764b574d2e2dc4ce4e1d02eaa.jpg|http:\\/\\/www.spring-and.cn\\/upload\\/20211229\\/8f3824585985cc3c004ba19d5d6a7861.jpg\",\"describe\":\"&lt;p&gt;这是对于碳酸泉头皮卸妆的描述&lt;\\/p&gt;\\n\",\"time_span\":\"6\",\"service_time\":\"10:00 - 22:00\",\"market_price\":\"389\",\"discount_price\":\"\",\"virtual_sales\":\"20\",\"sort\":\"0\",\"remark\":\"\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36', 1640764796);
INSERT INTO `sa_system_log_202112` VALUES (678, NULL, '/admin/login/index.html', 'post', '', '{\"username\":\"admin\",\"password\":\"***********\",\"captcha\":\"ppez\",\"keep_login\":\"0\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36', 1640764944);
INSERT INTO `sa_system_log_202112` VALUES (679, NULL, '/admin/login/index.html', 'post', '', '{\"username\":\"admin\",\"password\":\"***********\",\"captcha\":\"ey26\",\"keep_login\":\"0\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36', 1640773092);

-- ----------------------------
-- Table structure for sa_system_menu
-- ----------------------------
DROP TABLE IF EXISTS `sa_system_menu`;
CREATE TABLE `sa_system_menu`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `pid` bigint(20) UNSIGNED NOT NULL DEFAULT 0 COMMENT '父id',
  `title` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '名称',
  `icon` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '菜单图标',
  `href` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '链接',
  `params` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '链接参数',
  `target` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '_self' COMMENT '链接打开方式',
  `sort` int(11) NULL DEFAULT 0 COMMENT '菜单排序',
  `status` tinyint(1) UNSIGNED NOT NULL DEFAULT 1 COMMENT '状态(0:禁用,1:启用)',
  `remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `create_time` int(11) NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` int(11) NULL DEFAULT NULL COMMENT '更新时间',
  `delete_time` int(11) NULL DEFAULT NULL COMMENT '删除时间',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `title`(`title`) USING BTREE,
  INDEX `href`(`href`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 262 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '系统菜单表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sa_system_menu
-- ----------------------------
INSERT INTO `sa_system_menu` VALUES (227, 99999999, '后台首页', 'fa fa-home', 'index/welcome', '', '_self', 0, 1, NULL, NULL, 1573120497, NULL);
INSERT INTO `sa_system_menu` VALUES (228, 0, '系统管理', 'fa fa-cog', '', '', '_self', 0, 1, '', NULL, 1588999529, NULL);
INSERT INTO `sa_system_menu` VALUES (234, 228, '菜单管理', 'fa fa-tree', 'system.menu/index', '', '_self', 10, 1, '', NULL, 1588228555, NULL);
INSERT INTO `sa_system_menu` VALUES (244, 228, '管理员管理', 'fa fa-user', 'system.admin/index', '', '_self', 12, 1, '', 1573185011, 1588228573, NULL);
INSERT INTO `sa_system_menu` VALUES (245, 228, '角色管理', 'fa fa-bitbucket-square', 'system.auth/index', '', '_self', 11, 1, '', 1573435877, 1588228634, NULL);
INSERT INTO `sa_system_menu` VALUES (246, 228, '节点管理', 'fa fa-list', 'system.node/index', '', '_self', 9, 1, '', 1573435919, 1588228648, NULL);
INSERT INTO `sa_system_menu` VALUES (247, 228, '配置管理', 'fa fa-asterisk', 'system.config/index', '', '_self', 8, 1, '', 1573457448, 1588228566, NULL);
INSERT INTO `sa_system_menu` VALUES (248, 228, '上传管理', 'fa fa-arrow-up', 'system.uploadfile/index', '', '_self', 0, 1, '', 1573542953, 1588228043, NULL);
INSERT INTO `sa_system_menu` VALUES (249, 0, '商城管理', 'fa fa-list', '', '', '_self', 0, 1, '', 1589439884, 1589439884, NULL);
INSERT INTO `sa_system_menu` VALUES (250, 249, '商品分类', 'fa fa-calendar-check-o', 'mall.cate/index', '', '_self', 0, 1, '', 1589439910, 1589439966, NULL);
INSERT INTO `sa_system_menu` VALUES (251, 249, '商品管理', 'fa fa-list', 'mall.goods/index', '', '_self', 0, 1, '', 1589439931, 1589439942, NULL);
INSERT INTO `sa_system_menu` VALUES (252, 228, '快捷入口', 'fa fa-list', 'system.quick/index', '', '_self', 0, 1, '', 1589623683, 1589623683, NULL);
INSERT INTO `sa_system_menu` VALUES (253, 228, '日志管理', 'fa fa-connectdevelop', 'system.log/index', '', '_self', 0, 1, '', 1589623684, 1589623684, NULL);
INSERT INTO `sa_system_menu` VALUES (254, 0, '会员管理', 'fa fa-user', '', '', '_self', 0, 1, '', 1639820555, 1639820599, NULL);
INSERT INTO `sa_system_menu` VALUES (255, 254, '会员列表', 'fa fa-users', 'member.user/index', '', '_self', 0, 1, '', 1639820674, 1639820674, NULL);
INSERT INTO `sa_system_menu` VALUES (256, 0, '预约管理', 'fa fa-calendar', '', '', '_self', 0, 1, '', 1639923793, 1639923905, NULL);
INSERT INTO `sa_system_menu` VALUES (257, 256, '预约记录', 'fa fa-calendar-check-o', 'appointment.record/index', '', '_self', 0, 1, '', 1639923890, 1639923890, NULL);
INSERT INTO `sa_system_menu` VALUES (258, 0, '订单管理', 'fa fa-shopping-cart', '', '', '_self', 0, 1, '', 1639967962, 1639967962, NULL);
INSERT INTO `sa_system_menu` VALUES (259, 258, '订单列表', 'fa fa-shopping-bag', 'order.lists/index', '', '_self', 0, 1, '', 1639967986, 1639967986, NULL);
INSERT INTO `sa_system_menu` VALUES (260, 249, '服务管理', 'fa fa-hourglass', 'mall.service/index', '', '_self', 0, 1, '', 1640600827, 1640600827, NULL);
INSERT INTO `sa_system_menu` VALUES (261, 249, '卡项管理', 'fa fa-credit-card-alt', 'mall.card/index', '', '_self', 0, 1, '', 1640655783, 1640655783, NULL);

-- ----------------------------
-- Table structure for sa_system_node
-- ----------------------------
DROP TABLE IF EXISTS `sa_system_node`;
CREATE TABLE `sa_system_node`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `node` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '节点代码',
  `title` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '节点标题',
  `type` tinyint(1) NULL DEFAULT 3 COMMENT '节点类型（1：控制器，2：节点）',
  `is_auth` tinyint(1) UNSIGNED NULL DEFAULT 1 COMMENT '是否启动RBAC权限控制',
  `create_time` int(10) NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` int(10) NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `node`(`node`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 104 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '系统节点表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sa_system_node
-- ----------------------------
INSERT INTO `sa_system_node` VALUES (1, 'system.admin', '管理员管理', 1, 1, 1589580432, 1589580432);
INSERT INTO `sa_system_node` VALUES (2, 'system.admin/index', '列表', 2, 1, 1589580432, 1589580432);
INSERT INTO `sa_system_node` VALUES (3, 'system.admin/add', '添加', 2, 1, 1589580432, 1589580432);
INSERT INTO `sa_system_node` VALUES (4, 'system.admin/edit', '编辑', 2, 1, 1589580432, 1589580432);
INSERT INTO `sa_system_node` VALUES (5, 'system.admin/password', '编辑', 2, 1, 1589580432, 1589580432);
INSERT INTO `sa_system_node` VALUES (6, 'system.admin/delete', '删除', 2, 1, 1589580432, 1589580432);
INSERT INTO `sa_system_node` VALUES (7, 'system.admin/modify', '属性修改', 2, 1, 1589580432, 1589580432);
INSERT INTO `sa_system_node` VALUES (8, 'system.admin/export', '导出', 2, 1, 1589580432, 1589580432);
INSERT INTO `sa_system_node` VALUES (9, 'system.auth', '角色权限管理', 1, 1, 1589580432, 1589580432);
INSERT INTO `sa_system_node` VALUES (10, 'system.auth/authorize', '授权', 2, 1, 1589580432, 1589580432);
INSERT INTO `sa_system_node` VALUES (11, 'system.auth/saveAuthorize', '授权保存', 2, 1, 1589580432, 1589580432);
INSERT INTO `sa_system_node` VALUES (12, 'system.auth/index', '列表', 2, 1, 1589580432, 1589580432);
INSERT INTO `sa_system_node` VALUES (13, 'system.auth/add', '添加', 2, 1, 1589580432, 1589580432);
INSERT INTO `sa_system_node` VALUES (14, 'system.auth/edit', '编辑', 2, 1, 1589580432, 1589580432);
INSERT INTO `sa_system_node` VALUES (15, 'system.auth/delete', '删除', 2, 1, 1589580432, 1589580432);
INSERT INTO `sa_system_node` VALUES (16, 'system.auth/export', '导出', 2, 1, 1589580432, 1589580432);
INSERT INTO `sa_system_node` VALUES (17, 'system.auth/modify', '属性修改', 2, 1, 1589580432, 1589580432);
INSERT INTO `sa_system_node` VALUES (18, 'system.config', '系统配置管理', 1, 1, 1589580432, 1589580432);
INSERT INTO `sa_system_node` VALUES (19, 'system.config/index', '列表', 2, 1, 1589580432, 1589580432);
INSERT INTO `sa_system_node` VALUES (20, 'system.config/save', '保存', 2, 1, 1589580432, 1589580432);
INSERT INTO `sa_system_node` VALUES (21, 'system.menu', '菜单管理', 1, 1, 1589580432, 1589580432);
INSERT INTO `sa_system_node` VALUES (22, 'system.menu/index', '列表', 2, 1, 1589580432, 1589580432);
INSERT INTO `sa_system_node` VALUES (23, 'system.menu/add', '添加', 2, 1, 1589580432, 1589580432);
INSERT INTO `sa_system_node` VALUES (24, 'system.menu/edit', '编辑', 2, 1, 1589580432, 1589580432);
INSERT INTO `sa_system_node` VALUES (25, 'system.menu/delete', '删除', 2, 1, 1589580432, 1589580432);
INSERT INTO `sa_system_node` VALUES (26, 'system.menu/modify', '属性修改', 2, 1, 1589580432, 1589580432);
INSERT INTO `sa_system_node` VALUES (27, 'system.menu/getMenuTips', '添加菜单提示', 2, 1, 1589580432, 1589580432);
INSERT INTO `sa_system_node` VALUES (28, 'system.menu/export', '导出', 2, 1, 1589580432, 1589580432);
INSERT INTO `sa_system_node` VALUES (29, 'system.node', '系统节点管理', 1, 1, 1589580432, 1589580432);
INSERT INTO `sa_system_node` VALUES (30, 'system.node/index', '列表', 2, 1, 1589580432, 1589580432);
INSERT INTO `sa_system_node` VALUES (31, 'system.node/refreshNode', '系统节点更新', 2, 1, 1589580432, 1589580432);
INSERT INTO `sa_system_node` VALUES (32, 'system.node/clearNode', '清除失效节点', 2, 1, 1589580432, 1589580432);
INSERT INTO `sa_system_node` VALUES (33, 'system.node/add', '添加', 2, 1, 1589580432, 1589580432);
INSERT INTO `sa_system_node` VALUES (34, 'system.node/edit', '编辑', 2, 1, 1589580432, 1589580432);
INSERT INTO `sa_system_node` VALUES (35, 'system.node/delete', '删除', 2, 1, 1589580432, 1589580432);
INSERT INTO `sa_system_node` VALUES (36, 'system.node/export', '导出', 2, 1, 1589580432, 1589580432);
INSERT INTO `sa_system_node` VALUES (37, 'system.node/modify', '属性修改', 2, 1, 1589580432, 1589580432);
INSERT INTO `sa_system_node` VALUES (38, 'system.uploadfile', '上传文件管理', 1, 1, 1589580432, 1589580432);
INSERT INTO `sa_system_node` VALUES (39, 'system.uploadfile/index', '列表', 2, 1, 1589580432, 1589580432);
INSERT INTO `sa_system_node` VALUES (40, 'system.uploadfile/add', '添加', 2, 1, 1589580432, 1589580432);
INSERT INTO `sa_system_node` VALUES (41, 'system.uploadfile/edit', '编辑', 2, 1, 1589580432, 1589580432);
INSERT INTO `sa_system_node` VALUES (42, 'system.uploadfile/delete', '删除', 2, 1, 1589580432, 1589580432);
INSERT INTO `sa_system_node` VALUES (43, 'system.uploadfile/export', '导出', 2, 1, 1589580432, 1589580432);
INSERT INTO `sa_system_node` VALUES (44, 'system.uploadfile/modify', '属性修改', 2, 1, 1589580432, 1589580432);
INSERT INTO `sa_system_node` VALUES (45, 'mall.cate', '商品分类管理', 1, 1, 1589580432, 1589580432);
INSERT INTO `sa_system_node` VALUES (46, 'mall.cate/index', '列表', 2, 1, 1589580432, 1589580432);
INSERT INTO `sa_system_node` VALUES (47, 'mall.cate/add', '添加', 2, 1, 1589580432, 1589580432);
INSERT INTO `sa_system_node` VALUES (48, 'mall.cate/edit', '编辑', 2, 1, 1589580432, 1589580432);
INSERT INTO `sa_system_node` VALUES (49, 'mall.cate/delete', '删除', 2, 1, 1589580432, 1589580432);
INSERT INTO `sa_system_node` VALUES (50, 'mall.cate/export', '导出', 2, 1, 1589580432, 1589580432);
INSERT INTO `sa_system_node` VALUES (51, 'mall.cate/modify', '属性修改', 2, 1, 1589580432, 1589580432);
INSERT INTO `sa_system_node` VALUES (52, 'mall.goods', '商城商品管理', 1, 1, 1589580432, 1589580432);
INSERT INTO `sa_system_node` VALUES (53, 'mall.goods/index', '列表', 2, 1, 1589580432, 1589580432);
INSERT INTO `sa_system_node` VALUES (54, 'mall.goods/stock', '入库', 2, 1, 1589580432, 1589580432);
INSERT INTO `sa_system_node` VALUES (55, 'mall.goods/add', '添加', 2, 1, 1589580432, 1589580432);
INSERT INTO `sa_system_node` VALUES (56, 'mall.goods/edit', '编辑', 2, 1, 1589580432, 1589580432);
INSERT INTO `sa_system_node` VALUES (57, 'mall.goods/delete', '删除', 2, 1, 1589580432, 1589580432);
INSERT INTO `sa_system_node` VALUES (58, 'mall.goods/export', '导出', 2, 1, 1589580432, 1589580432);
INSERT INTO `sa_system_node` VALUES (59, 'mall.goods/modify', '属性修改', 2, 1, 1589580432, 1589580432);
INSERT INTO `sa_system_node` VALUES (60, 'system.quick', '快捷入口管理', 1, 1, 1589623188, 1589623188);
INSERT INTO `sa_system_node` VALUES (61, 'system.quick/index', '列表', 2, 1, 1589623188, 1589623188);
INSERT INTO `sa_system_node` VALUES (62, 'system.quick/add', '添加', 2, 1, 1589623188, 1589623188);
INSERT INTO `sa_system_node` VALUES (63, 'system.quick/edit', '编辑', 2, 1, 1589623188, 1589623188);
INSERT INTO `sa_system_node` VALUES (64, 'system.quick/delete', '删除', 2, 1, 1589623188, 1589623188);
INSERT INTO `sa_system_node` VALUES (65, 'system.quick/export', '导出', 2, 1, 1589623188, 1589623188);
INSERT INTO `sa_system_node` VALUES (66, 'system.quick/modify', '属性修改', 2, 1, 1589623188, 1589623188);
INSERT INTO `sa_system_node` VALUES (67, 'system.log', '操作日志管理', 1, 1, 1589623188, 1589623188);
INSERT INTO `sa_system_node` VALUES (68, 'system.log/index', '列表', 2, 1, 1589623188, 1589623188);
INSERT INTO `sa_system_node` VALUES (69, 'member.user', '会员列表', 1, 1, 1639820464, 1639923601);
INSERT INTO `sa_system_node` VALUES (70, 'member.user/index', '列表', 2, 1, 1639820464, 1639820464);
INSERT INTO `sa_system_node` VALUES (71, 'member.user/add', '添加', 2, 1, 1639820464, 1639820464);
INSERT INTO `sa_system_node` VALUES (72, 'member.user/edit', '编辑', 2, 1, 1639820464, 1639820464);
INSERT INTO `sa_system_node` VALUES (73, 'member.user/delete', '删除', 2, 1, 1639820464, 1639820464);
INSERT INTO `sa_system_node` VALUES (74, 'member.user/export', '导出', 2, 1, 1639820464, 1639820464);
INSERT INTO `sa_system_node` VALUES (75, 'member.user/modify', '属性修改', 2, 1, 1639820464, 1639820464);
INSERT INTO `sa_system_node` VALUES (76, 'appointment.record', '预约记录', 1, 1, 1639923405, 1639923592);
INSERT INTO `sa_system_node` VALUES (77, 'appointment.record/index', '列表', 2, 1, 1639923405, 1639923405);
INSERT INTO `sa_system_node` VALUES (78, 'appointment.record/add', '添加', 2, 1, 1639923405, 1639923405);
INSERT INTO `sa_system_node` VALUES (79, 'appointment.record/edit', '编辑', 2, 1, 1639923405, 1639923405);
INSERT INTO `sa_system_node` VALUES (80, 'appointment.record/delete', '删除', 2, 1, 1639923405, 1639923405);
INSERT INTO `sa_system_node` VALUES (81, 'appointment.record/export', '导出', 2, 1, 1639923405, 1639923405);
INSERT INTO `sa_system_node` VALUES (82, 'appointment.record/modify', '属性修改', 2, 1, 1639923405, 1639923405);
INSERT INTO `sa_system_node` VALUES (83, 'order.lists', '订单列表', 1, 1, 1639967919, 1639967919);
INSERT INTO `sa_system_node` VALUES (84, 'order.lists/index', '列表', 2, 1, 1639967919, 1639967919);
INSERT INTO `sa_system_node` VALUES (85, 'order.lists/add', '添加', 2, 1, 1639967919, 1639967919);
INSERT INTO `sa_system_node` VALUES (86, 'order.lists/edit', '编辑', 2, 1, 1639967919, 1639967919);
INSERT INTO `sa_system_node` VALUES (87, 'order.lists/delete', '删除', 2, 1, 1639967919, 1639967919);
INSERT INTO `sa_system_node` VALUES (88, 'order.lists/export', '导出', 2, 1, 1639967919, 1639967919);
INSERT INTO `sa_system_node` VALUES (89, 'order.lists/modify', '属性修改', 2, 1, 1639967919, 1639967919);
INSERT INTO `sa_system_node` VALUES (90, 'mall.service', '商城服务管理', 1, 1, 1640600675, 1640600675);
INSERT INTO `sa_system_node` VALUES (91, 'mall.service/index', '列表', 2, 1, 1640600675, 1640600675);
INSERT INTO `sa_system_node` VALUES (92, 'mall.service/add', '添加', 2, 1, 1640600675, 1640600675);
INSERT INTO `sa_system_node` VALUES (93, 'mall.service/edit', '编辑', 2, 1, 1640600675, 1640600675);
INSERT INTO `sa_system_node` VALUES (94, 'mall.service/delete', '删除', 2, 1, 1640600675, 1640600675);
INSERT INTO `sa_system_node` VALUES (95, 'mall.service/export', '导出', 2, 1, 1640600675, 1640600675);
INSERT INTO `sa_system_node` VALUES (96, 'mall.service/modify', '属性修改', 2, 1, 1640600675, 1640600675);
INSERT INTO `sa_system_node` VALUES (97, 'mall.card', '商城卡项管理', 1, 1, 1640655751, 1640655751);
INSERT INTO `sa_system_node` VALUES (98, 'mall.card/index', '列表', 2, 1, 1640655751, 1640655751);
INSERT INTO `sa_system_node` VALUES (99, 'mall.card/add', '添加', 2, 1, 1640655751, 1640655751);
INSERT INTO `sa_system_node` VALUES (100, 'mall.card/edit', '编辑', 2, 1, 1640655751, 1640655751);
INSERT INTO `sa_system_node` VALUES (101, 'mall.card/delete', '删除', 2, 1, 1640655751, 1640655751);
INSERT INTO `sa_system_node` VALUES (102, 'mall.card/export', '导出', 2, 1, 1640655751, 1640655751);
INSERT INTO `sa_system_node` VALUES (103, 'mall.card/modify', '属性修改', 2, 1, 1640655751, 1640655751);

-- ----------------------------
-- Table structure for sa_system_quick
-- ----------------------------
DROP TABLE IF EXISTS `sa_system_quick`;
CREATE TABLE `sa_system_quick`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `title` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '快捷入口名称',
  `icon` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '图标',
  `href` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '快捷链接',
  `sort` int(11) NULL DEFAULT 0 COMMENT '排序',
  `status` tinyint(1) UNSIGNED NULL DEFAULT 1 COMMENT '状态(1:禁用,2:启用)',
  `remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注说明',
  `create_time` int(11) NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` int(11) NULL DEFAULT NULL COMMENT '更新时间',
  `delete_time` int(11) NULL DEFAULT NULL COMMENT '删除时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 12 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '系统快捷入口表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sa_system_quick
-- ----------------------------
INSERT INTO `sa_system_quick` VALUES (1, '管理员管理', 'fa fa-user', 'system.admin/index', 0, 1, '', 1589624097, 1589624792, NULL);
INSERT INTO `sa_system_quick` VALUES (2, '角色管理', 'fa fa-bitbucket-square', 'system.auth/index', 0, 1, '', 1589624772, 1589624781, NULL);
INSERT INTO `sa_system_quick` VALUES (3, '菜单管理', 'fa fa-tree', 'system.menu/index', 0, 1, NULL, 1589624097, 1589624792, NULL);
INSERT INTO `sa_system_quick` VALUES (6, '节点管理', 'fa fa-list', 'system.node/index', 0, 1, NULL, 1589624772, 1589624781, NULL);
INSERT INTO `sa_system_quick` VALUES (7, '配置管理', 'fa fa-asterisk', 'system.config/index', 0, 1, NULL, 1589624097, 1589624792, NULL);
INSERT INTO `sa_system_quick` VALUES (8, '上传管理', 'fa fa-arrow-up', 'system.uploadfile/index', 0, 1, NULL, 1589624772, 1589624781, NULL);
INSERT INTO `sa_system_quick` VALUES (10, '商品分类', 'fa fa-calendar-check-o', 'mall.cate/index', 0, 1, NULL, 1589624097, 1589624792, NULL);
INSERT INTO `sa_system_quick` VALUES (11, '商品管理', 'fa fa-list', 'mall.goods/index', 0, 1, NULL, 1589624772, 1589624781, NULL);

-- ----------------------------
-- Table structure for sa_system_uploadfile
-- ----------------------------
DROP TABLE IF EXISTS `sa_system_uploadfile`;
CREATE TABLE `sa_system_uploadfile`  (
  `id` int(20) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `upload_type` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'local' COMMENT '存储位置',
  `original_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '文件原名',
  `url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '物理路径',
  `image_width` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '宽度',
  `image_height` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '高度',
  `image_type` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '图片类型',
  `image_frames` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '图片帧数',
  `mime_type` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT 'mime类型',
  `file_size` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '文件大小',
  `file_ext` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `sha1` varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '文件 sha1编码',
  `create_time` int(10) NULL DEFAULT NULL COMMENT '创建日期',
  `update_time` int(10) NULL DEFAULT NULL COMMENT '更新时间',
  `upload_time` int(10) NULL DEFAULT NULL COMMENT '上传时间',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `upload_type`(`upload_type`) USING BTREE,
  INDEX `original_name`(`original_name`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 306 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '上传文件表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sa_system_uploadfile
-- ----------------------------
INSERT INTO `sa_system_uploadfile` VALUES (286, 'alioss', 'image/jpeg', 'https://lxn-99php.oss-cn-shenzhen.aliyuncs.com/upload/20191111/0a6de1ac058ee134301501899b84ecb1.jpg', '', '', '', 0, 'image/jpeg', 0, 'jpg', '', NULL, NULL, NULL);
INSERT INTO `sa_system_uploadfile` VALUES (287, 'alioss', 'image/jpeg', 'https://lxn-99php.oss-cn-shenzhen.aliyuncs.com/upload/20191111/46d7384f04a3bed331715e86a4095d15.jpg', '', '', '', 0, 'image/jpeg', 0, 'jpg', '', NULL, NULL, NULL);
INSERT INTO `sa_system_uploadfile` VALUES (288, 'alioss', 'image/x-icon', 'https://lxn-99php.oss-cn-shenzhen.aliyuncs.com/upload/20191111/7d32671f4c1d1b01b0b28f45205763f9.ico', '', '', '', 0, 'image/x-icon', 0, 'ico', '', NULL, NULL, NULL);
INSERT INTO `sa_system_uploadfile` VALUES (289, 'alioss', 'image/jpeg', 'https://lxn-99php.oss-cn-shenzhen.aliyuncs.com/upload/20191111/28cefa547f573a951bcdbbeb1396b06f.jpg', '', '', '', 0, 'image/jpeg', 0, 'jpg', '', NULL, NULL, NULL);
INSERT INTO `sa_system_uploadfile` VALUES (290, 'alioss', 'image/jpeg', 'https://lxn-99php.oss-cn-shenzhen.aliyuncs.com/upload/20191111/2c412adf1b30c8be3a913e603c7b6e4a.jpg', '', '', '', 0, 'image/jpeg', 0, 'jpg', '', NULL, NULL, NULL);
INSERT INTO `sa_system_uploadfile` VALUES (291, 'alioss', 'timg (1).jpg', 'http://easyadmin.oss-cn-shenzhen.aliyuncs.com/upload/20191113/ff793ced447febfa9ea2d86f9f88fa8e.jpg', '', '', '', 0, 'image/jpeg', 0, 'jpg', '', 1573612437, NULL, NULL);
INSERT INTO `sa_system_uploadfile` VALUES (296, 'txcos', '22243.jpg', 'https://easyadmin-1251997243.cos.ap-guangzhou.myqcloud.com/upload/20191114/2381eaf81208ac188fa994b6f2579953.jpg', '', '', '', 0, 'image/jpeg', 0, 'jpg', '', 1573712153, NULL, NULL);
INSERT INTO `sa_system_uploadfile` VALUES (297, 'local', 'timg.jpg', 'http://admin.host/upload/20200423/5055a273cf8e3f393d699d622b74f247.jpg', '', '', '', 0, 'image/jpeg', 0, 'jpg', '', 1587614155, NULL, NULL);
INSERT INTO `sa_system_uploadfile` VALUES (298, 'local', 'timg.jpg', 'http://admin.host/upload/20200423/243f4e59f1b929951ef79c5f8be7468a.jpg', '', '', '', 0, 'image/jpeg', 0, 'jpg', '', 1587614269, NULL, NULL);
INSERT INTO `sa_system_uploadfile` VALUES (299, 'local', 'head.jpg', 'http://admin.host/upload/20200512/a5ce9883379727324f5686ef61205ce2.jpg', '', '', '', 0, 'image/jpeg', 0, 'jpg', '', 1589255649, NULL, NULL);
INSERT INTO `sa_system_uploadfile` VALUES (300, 'local', '896e5b87c9ca70e4.jpg', 'http://admin.host/upload/20200514/577c65f101639f53dbbc9e7aa346f81c.jpg', '', '', '', 0, 'image/jpeg', 0, 'jpg', '', 1589427798, NULL, NULL);
INSERT INTO `sa_system_uploadfile` VALUES (301, 'local', '896e5b87c9ca70e4.jpg', 'http://admin.host/upload/20200514/98fc09b0c4ad4d793a6f04bef79a0edc.jpg', '', '', '', 0, 'image/jpeg', 0, 'jpg', '', 1589427840, NULL, NULL);
INSERT INTO `sa_system_uploadfile` VALUES (302, 'local', '18811e7611c8f292.jpg', 'http://admin.host/upload/20200514/e1c6c9ef6a4b98b8f7d95a1a0191a2df.jpg', '', '', '', 0, 'image/jpeg', 0, 'jpg', '', 1589438645, NULL, NULL);
INSERT INTO `sa_system_uploadfile` VALUES (303, 'local', '1.jpg', 'http://www.spring-and.cn/upload/20211229/500452d863b879d6487a319015657e80.jpg', '', '', '', 0, 'image/jpeg', 0, 'jpg', '', 1640764734, NULL, NULL);
INSERT INTO `sa_system_uploadfile` VALUES (304, 'local', '2.jpg', 'http://www.spring-and.cn/upload/20211229/0ca704a764b574d2e2dc4ce4e1d02eaa.jpg', '', '', '', 0, 'image/jpeg', 0, 'jpg', '', 1640764746, NULL, NULL);
INSERT INTO `sa_system_uploadfile` VALUES (305, 'local', '1.jpg', 'http://www.spring-and.cn/upload/20211229/8f3824585985cc3c004ba19d5d6a7861.jpg', '', '', '', 0, 'image/jpeg', 0, 'jpg', '', 1640764746, NULL, NULL);

SET FOREIGN_KEY_CHECKS = 1;
